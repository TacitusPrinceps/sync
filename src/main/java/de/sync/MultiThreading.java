package de.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Project: sync
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 11/18/17
 */
public class MultiThreading {

    private static ExecutorService executor = Executors.newFixedThreadPool(10);


    public static void newThread(Runnable run){
        executor.submit(run);
    }

}
