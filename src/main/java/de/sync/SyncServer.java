package de.sync;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Project: sync
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 11/18/17
 */
public class SyncServer {

    private ServerSocket serverSocket;

    public void start() {
        try {
            serverSocket = new ServerSocket(61101);
            System.out.println("Server started!");

            MultiThreading.newThread(this::handleServer);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleServer() {
        while (true) {
            Socket accept = null;
            int bytesRead;
            try {
                accept = serverSocket.accept();



                // Read incoming files and save them to disk
                InputStream in = accept.getInputStream();

                DataInputStream clientData = new DataInputStream(in);

                String fileName = clientData.readUTF();
                OutputStream output = new FileOutputStream("server/" + fileName);
                long size = clientData.readLong();
                byte[] buffer = new byte[1024];
                while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
                    output.write(buffer, 0, bytesRead);
                    size -= bytesRead;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Found new client: " + accept);
        }
    }

}
