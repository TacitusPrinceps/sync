package de.sync;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Project: sync
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 11/18/17
 */
public class SyncClient {

    private Socket socket;

    public void connect() {
        //TODO implement proper subnet recognition
        String subnet = "192.168.2";
        int timeout = 10;
        for (int i = 1; i < 255; i++) {
            String host = subnet + "." + i;
            try {
                if (InetAddress.getByName(host).isReachable(timeout)) {
                    MultiThreading.newThread(() -> connectTo(host));
                }
            } catch (IOException ignored) {

            }
        }
    }

    private void connectTo(String host) {
        try {
            this.socket = new Socket(host, 61101);
            System.out.println("Connected to server " + host);

        } catch (IOException ignored) {
        }
    }


    // is supposed to send the file to the server
    public void sync(File toTransfer) {
        byte[] mybytearray = new byte[(int) toTransfer.length()];
        try {
            FileInputStream fis = new FileInputStream(toTransfer);
            BufferedInputStream bis = new BufferedInputStream(fis);

            DataInputStream dis = new DataInputStream(bis);
            dis.readFully(mybytearray, 0, mybytearray.length);

            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(toTransfer.getName());
            dos.writeLong(mybytearray.length);
            dos.write(mybytearray, 0, mybytearray.length);
            dos.flush();
        } catch (IOException ignored) {

        }
    }
}
