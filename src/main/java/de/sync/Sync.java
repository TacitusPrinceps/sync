package de.sync;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.Enumeration;

import static java.net.InetAddress.getLocalHost;


public class Sync {

    public static void main(String... args) throws UnknownHostException {
        Sync sync = new Sync();
        byte[] ownIp = getLocalHost().getAddress();
        System.out.println(Arrays.toString(ownIp));
        sync.checkHosts("192.168.2");
    }

    public void checkHosts(String subnet) {
        int timeout = 10;
        for (int i = 1; i < 255; i++) {
            String host = subnet + "." + i;
            try {
                if (InetAddress.getByName(host).isReachable(timeout)) {
                    System.out.println(host + " is reachable");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getIpAddress() throws UnknownHostException {
        try {
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            for (; n.hasMoreElements(); ) {
                NetworkInterface e = n.nextElement();
                Enumeration<InetAddress> a = e.getInetAddresses();
                for (; a.hasMoreElements(); ) {
                    InetAddress addr = a.nextElement();
                    if (addr instanceof Inet4Address) {
                        if (!(addr.getHostAddress().equals("127.0.0.1"))) {
                            return addr.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException socketException) {
            socketException.printStackTrace();
        }
        String s = "";
        return s;
    }
}
