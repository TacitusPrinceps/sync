package de.sync;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Project: sync
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 11/18/17
 */
public class TestSync {

    public static void main(String... args) {

        TestSync t = new TestSync();
        SyncServer ss;
        SyncClient client = null;


        File toTransfer = new File("client/Blatt05.pdf");

        boolean isTerminated = false;

        while (!isTerminated) {
            switch (t.readInt("Please enter your operation\n1: Start local server\n2: Connect to a server\n3: Sync File from client to server\n10: Quit")) {
                case 1:
                    ss = new SyncServer();
                    ss.start();
                    break;

                case 2:
                    client = new SyncClient();
                    client.connect();

                    break;

                case 3:
                    if (client != null)
                        client.sync(toTransfer);
                    break;

                case 10:
                    System.out.println("bye");
                    isTerminated = true;
            }
        }
    }


    public String readString(String text) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(text);
        String s = null;
        try {
            s = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    public int readInt(String text) {
        System.out.print(text);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String s = br.readLine();
            int i = Integer.parseInt(s);
            return i;
        } catch (IOException e) {
            System.out.println("Invalid integer!");
            return -1;
        }
    }

}
